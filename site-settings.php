<?php

return [
    'config_dir'  => 'novum.jenv',
    'namespace'   => 'NovumJenv',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'justitie.demo.novum.nu',
    'dev_domain'  => 'justitie.demo.novum.nuidev.nl',
    'test_domain' => 'justitie.test.demo.novum.nu',
];
